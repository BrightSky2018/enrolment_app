<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Registration;
use App\Course;
 
final class CoursesController extends Controller
{
    /**
     * List all courses.
     *
     * @param  Request  $request
     * @return Response
     */
    public function listCourses(Request $request)
    {
        /**
         * It is better to get JOIN results and then iterate
         * over Courses adding extra attribute, than to run
         * SELECT corresponding courses from the registrations
         * table, for each course and to add as appended attribute.
         * 
         * Current, approach will always be faster and scale better,
         * since $registrations and $courses can be cached.
         * Ultimately, it is - speed vs space dilemma :) can not get both.
         */
        $registrations = Registration::getAllRegistrationsWithCourses();
        $courses = Course::all();

        foreach ($courses as $key => $course) {
            $course->available = $course->capacity > $registrations->where('course_id', $course->id)->count();
        }

        return response()->json($courses);
    }
}