<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Registration;
use Carbon\Carbon;
use App\Exceptions\EnrolmentException;

final class EnrolmentController extends Controller
{
    /**
     * List all enrolments.
     *
     * @param  Request  $request
     * @return Response
     */
    public function listEnrolments(Request $request)
    {
        return response()->json(['data' => Registration::all()]);
    }

    /**
     * Create new enrolment.
     *
     * @param  Request  $request
     * @return Response
     */
    public function enrol(Request $request)
    {
        $result = null;

        try {
            $registrations = Registration::getCourseWithRegistrations($request->input('course_id'));

            if (!$registrations->isEmpty()) {
                /**
                 * All the querying activities can be orgonized and
                 * placed inside the Repository pattern, however due to
                 * the lack of time I did not implement it.
                 */
                $capacity = $registrations->where('course_id', $request->input('course_id'))->first()->capacity;
                $registred = $registrations->count();
                $enrolSameCourse = $registrations->where('course_id', $request->input('course_id'))
                    ->where('student_id', $request->input('student_id'))
                    ->count();

                if ($capacity <= $registred) {
                    throw new EnrolmentException('Course is unavailable');
                }

                if ($enrolSameCourse) {
                    throw new EnrolmentException('Student can not enrol the same class twice');
                }
            }

            $registration = new Registration();
            $registration->fill([
                'student_id' => $request->input('student_id'),
                'course_id' => $request->input('course_id'),
                'registered_on' => Carbon::now()
            ]);
            $result = $registration->save();
        } catch (\Throwable $e) {
            //Log error and rethrow another exception or return with response
            $result = $e->getMessage();
        }

        return response()->json($result);
    }
}
