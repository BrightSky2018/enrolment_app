<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Registration extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id', 'course_id', 'registered_on'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'registered_on' => 'datetime',
    ];

    /**
     * Returns JOIN product of the registrations and courses tables.
     * 
     * @return Collection
     */
    public static function getAllRegistrationsWithCourses(): Collection
    {
        return Registration::join('courses', 'registrations.course_id', '=', 'courses.id')
            ->get(['registrations.course_id']);
    }

    /**
     * Function gets a course and all the registrations it is in. Would concider 
     * using it over the variant of having Registration with relation
     * $this->hasOne('App\Course', 'id', 'course_id'), since it would make extra SELECT  
     * query in addition to the SELECT registrations statement, thus queries db
     * twice or N+1, depending on how many registrations are selected.
     * 
     * NOTE: To get course available capacity, one could use 2 queries: 
     * to get course capacity and to count course registred times.
     * To avoid that, better to query JOIN results. Assuming that the number
     * of registrations per course is small, it is better to hit database once
     * and to get JOIN product, and then just filter the results.
     * Besides, the JOIN results also allow to perform quick cross check - 
     * if the same user is trying to book the same course twice and to prevent that,
     * despite using database constraint.
     * 
     * @param int $courseId
     * @return Collection
     */
    public static function getCourseWithRegistrations(int $courseId): Collection
    {
        return Registration::join('courses', 'registrations.course_id', '=', 'courses.id')
            ->where('course_id', $courseId)
            ->get(['registrations.*', 'courses.capacity']);
    }
}
