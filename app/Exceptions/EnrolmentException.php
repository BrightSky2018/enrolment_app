<?php

namespace App\Exceptions;

use Exception;

final class EnrolmentException extends Exception
{
}