## About the solution
The current soluton is fully implemented using Laravel framework and its official documentation. All of the code there is self explanatory.
I would elaborate on the project if it had custom solutions, vanilla PHP and diagrams. In this case I do not know if there is anything I can say :).

## Running the solution
- Install mysql
- Create database `university`
- Edit `.env` file settings
- Run project `php -S localhost:<your port>`
- Migrate database schema `php artisan migrate`
- Populate students table `php artisan db:seed --class=StudentSeeder`
- Populate courses table `php artisan db:seed --class=CoursesSeeder`
- Enrol students - `curl -X POST -F 'student_id=1...25' -F 'course_id=1...15' http://localhost:<your port>/enrol`
- List courses - `curl -X GET  http://localhost:<your port>/courses`
- List enrolments - `curl -X GET  http://localhost:<your port>/enrolments`
