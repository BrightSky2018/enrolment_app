<?php

use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Student::class, 25)->create()->each(function ($student) {
            $student->save(factory(App\Student::class)->make());
        });
    }
}
